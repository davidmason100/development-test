﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace development_test.Models
{
    public class PropertyDataViewModel
    {
        public List<PropertyDataItem> dataItems {get;set;}     
    }

    public class PropertyDataItem
    {
        public string ID { get; set; }
        public int SalePrice { get; set; }
        public string DisplayAddress { get; set; }
        public string SellingStatus { get; set; }
        public DateTime TimeStamp { get; set; }
    }
}
