﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using development_test.Models;
using DataService.Models;
using DataService;

namespace development_test.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index([FromServices] IDataStore dataStore)
        {
            PropertySalesModel dataModel = new PropertySalesModel();
            dataModel.items = dataStore.Get().ToList();

            PropertyDataViewModel viewModel = new PropertyDataViewModel();
            viewModel.dataItems = new List<PropertyDataItem>();

            foreach (var dataItem in dataModel.items)
            {
                PropertyDataItem viewModelItem = new PropertyDataItem();
                viewModelItem.ID = dataItem.id;
                viewModelItem.SalePrice = dataItem.sale_price;
                viewModelItem.SellingStatus = dataItem.selling_status;
                viewModelItem.TimeStamp = dataItem._timestamp;
                viewModelItem.DisplayAddress = dataItem.display_address;
                viewModel.dataItems.Add(viewModelItem);
            }

            return View(viewModel);
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "David Mason";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
