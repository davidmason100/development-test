﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using DataService;
using System.IO;

namespace development_test
{
    public class Startup
    {
        const string propertySalesDataFile = "PropertySalesData.json";

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string RunningPath = AppDomain.CurrentDomain.BaseDirectory;

            string propertyDataFilePath = string.Format("{0}Resources\\" + propertySalesDataFile, Path.GetFullPath(Path.Combine(RunningPath, @"..\..\..\")));
            //JsonDataStore jsonDataStore = new JsonDataStore(propertyDataFilePath);
            services.AddSingleton<IDataStore>((sp => new JsonDataStore(propertyDataFilePath)));
            
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseBrowserLink();
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
