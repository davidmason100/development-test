﻿using DataService.Models;
using System;
using System.Linq;


namespace DataService
{
    public interface IDataStore
    {
        IQueryable<PropertySalesItem> Get();

        void Add(PropertySalesItem item);

        void Update(PropertySalesItem item);
    }
}
