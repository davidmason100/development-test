﻿using DataService.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace DataService
{
    public class JsonDataStore : IDataStore
    {
        private readonly string dataFile;
        private readonly PropertySalesModel data;

        public JsonDataStore(string dataFile)
        {
            try
            {
                this.dataFile = dataFile;

                using (StreamReader r = new StreamReader(dataFile))
                {
                    string json = r.ReadToEnd();
                    data = JsonConvert.DeserializeObject<PropertySalesModel>(json);
                }
            }
            catch(Exception ex)
            {
                //Log error to file
            }
        }

        public IQueryable<PropertySalesItem> Get()
        {
            IQueryable<PropertySalesItem> returnedQueryable = Enumerable.Empty<PropertySalesItem>().AsQueryable();
            try
            {
                returnedQueryable = this.data.items.AsQueryable();
            }
            catch
            {
                //Log error to file
            }
            return returnedQueryable;
        }

        public void Add(PropertySalesItem item)
        {
            lock (this)
            {
                try
                {
                    // Make sure that only one thread is updating the file
                    this.data.items.Add(item);
                    string json = JsonConvert.SerializeObject(this.data);
                    File.WriteAllText(this.dataFile, json);
                }
                catch(Exception ex)
                {
                    //Log error to file
                }
            }
        }

        public void Update(PropertySalesItem item)
        {
            lock(this)
            {
                try
                {
                    var key = item.id.ToString();
                    for (int i = 0; i < this.data.items.Count; i++)
                    {
                        PropertySalesItem itemToUpdate = new PropertySalesItem();

                        if (this.data.items[i].id == key)
                        {
                            itemToUpdate = this.data.items[i];
                            itemToUpdate.sale_price = item.sale_price;
                            itemToUpdate.selling_status = item.selling_status;
                            itemToUpdate._timestamp = item._timestamp;
                            itemToUpdate.display_address = item.display_address;
                            string json = JsonConvert.SerializeObject(this.data);
                            File.WriteAllText(this.dataFile, json);
                            break;
                        }
                    }
                }
                catch(Exception ex)
                {
                    //Log error to file
                }
            }
        }

    }
}
