﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DataService.Models
{
    public class PropertySalesModel
    {
        public int total { get; set; }
        public List<PropertySalesItem> items { get; set; }
    }

    public class PropertySalesItem
    {
        public string id { get; set; }
        public int sale_price { get; set; }
        public string display_address { get; set; }
        public string selling_status { get; set; }
        public DateTime _timestamp { get; set; }
    }
}
